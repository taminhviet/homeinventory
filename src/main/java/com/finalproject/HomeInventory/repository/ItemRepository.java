package com.finalproject.HomeInventory.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.finalproject.HomeInventory.model.Items;

@Repository
public interface ItemRepository extends JpaRepository<Items, Integer>{
    
    @Transactional
    @Modifying
    @Query("SELECT i FROM Items i WHERE i.owner = ?1")
    public List<Items> findItemByOwner(String username);
}
