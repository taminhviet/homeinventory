package com.finalproject.HomeInventory.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.finalproject.HomeInventory.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    
    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.email = ?2, u.firstname = ?3, u.lastname = ?4 WHERE u.username = ?1")
    void updateUser(String Username,String email, String firstname, String lastname);
    
    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.active = ?2 WHERE u.username = ?1")
    void deactiveUser(String Username,int active);
    
    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.active = ?2 WHERE u.username = ?1")
    void activeUser(String Username,int active);
    
    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.isadmin = ?2 WHERE u.username = ?1")
    void promoteUser(String Username,int isAdmin);
    
    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.isadmin = ?2 WHERE u.username = ?1")
    void demoteUser(String Username,int isAdmin);
    
    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.password = ?2 WHERE u.username = ?1")
    void changePassword(String Username,String password);
}

