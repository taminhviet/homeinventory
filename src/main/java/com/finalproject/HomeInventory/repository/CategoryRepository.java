package com.finalproject.HomeInventory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.finalproject.HomeInventory.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{

    
}
