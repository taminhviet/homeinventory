package com.finalproject.HomeInventory.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name ="categories")
public class Category{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "categoryid", nullable = false)
    private int categoryid;
    
    @Column(name ="CategoryName")
    private String categoryname;

    @OneToOne(mappedBy = "category")
    private Items item;

    public Items getItem() {
        return item;
    }

    public void setItem(Items item) {
        this.item = item;
    }
    

    public Category(String categoryname) {
        super();
        this.categoryname = categoryname;
    }

    public Category() {
        super();
    }


    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }    
    
    @Override
    public String toString() {
        return "Category [categoryid= " +categoryid +",categoryname = " + categoryname +"]";
    }

}
