package com.finalproject.HomeInventory.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "users")
public class User {
    
    @Id
    @Column(name= "Username")
    private String username;
    
    @Column(name="Password")
    private String password;
    
    @Transient
    private String passwordConfirm;
    
    @Column(name="Email")
    private String email;
    
    @Column(name="FirstName")
    private String firstname;
    
    @Column(name="LastName")
    private String lastname;
    
    @Column(name="Active")
    private int active;
    
    @Column(name="IsAdmin")
    private int isadmin;
    

    public User() {
        super();
    }

    public User(String username, String password,
            String email, String firstname, String lastname, int active,
            int isadmin) {
        super();
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.active = active;
        this.isadmin = isadmin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getIsadmin() {
        return isadmin;
    }

    public void setIsadmin(int isadmin) {
        this.isadmin = isadmin;
    }
    
}