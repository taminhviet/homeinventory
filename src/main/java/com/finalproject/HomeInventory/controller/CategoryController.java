package com.finalproject.HomeInventory.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.finalproject.HomeInventory.model.Category;
import com.finalproject.HomeInventory.service.CategoryService;

@Controller
public class CategoryController {

    @Autowired
    CategoryService categoryService;
    
    @RequestMapping(value ="/admin/categories", method = RequestMethod.GET)
    public String category(Model model){
        List<Category> lsCategory = categoryService.getAllCategories();
        if(!lsCategory.isEmpty()){
            model.addAttribute("lsCategory", lsCategory);
        }
        return "/admin/category";
    }
    
    @RequestMapping(value="/admin/updatecategory", method = RequestMethod.POST)
    public String updateCategory(Model model,
            @RequestParam(value = "id", required = false) int id,
            @RequestParam(value = "Category name", required = false) String categoryname) {
        Category category = categoryService.findCategory(id);
        if(categoryname != null){
            category.setCategoryname(categoryname);
            categoryService.saveCategory(category);
        }
        return "redirect:/admin/categories";
    }

    @RequestMapping(value = "/admin/addcategory", method = RequestMethod.POST)
    public String addCategory(
            Model model,
            @RequestParam(value = "categoryname", required = false) String categoryname) {
        Category category = new Category();
        category.setCategoryname(categoryname);
        categoryService.saveCategory(category);
        return "redirect:/admin/categories";
    }
}
