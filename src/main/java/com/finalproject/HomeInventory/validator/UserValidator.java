package com.finalproject.HomeInventory.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.finalproject.HomeInventory.model.User;
import com.finalproject.HomeInventory.service.UserService;

@Component
public class UserValidator {
    @Autowired
    private UserService userService;

    public boolean existUser(String username){
        User user = userService.findByUsername(username);
        if(user != null){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean confirmPasswordValidate(String password, String confirmPassword){
        if(confirmPassword.equals(password)){
            return true;
        }else{
            return false;
        }
        
    }
}