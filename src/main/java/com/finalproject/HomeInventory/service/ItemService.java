package com.finalproject.HomeInventory.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalproject.HomeInventory.model.Items;
import com.finalproject.HomeInventory.repository.ItemRepository;

@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepository;
    
    public void saveItem(Items item){
        itemRepository.save(item);
    }
    
    public List<Items> getAllItem(){
        return itemRepository.findAll();
    }
    
    public List<Items> getItemByUser(String username){
        return itemRepository.findItemByOwner(username);
    }
    
    public void deleteItem(Items item){
        itemRepository.delete(item);
    }
    
    public Items findItem(int itemid){
        return itemRepository.findById(itemid).get();
    }
}
