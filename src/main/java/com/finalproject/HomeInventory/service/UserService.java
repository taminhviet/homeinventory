package com.finalproject.HomeInventory.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.finalproject.HomeInventory.Utility.Constants;
import com.finalproject.HomeInventory.model.User;
import com.finalproject.HomeInventory.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(Constants.ACTIVE);
        user.setIsadmin(Constants.ROLE_USER);
        userRepository.save(user);
    }
    public void savebyadmin(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(Constants.ACTIVE);
        userRepository.save(user);
    }
    
    public void updateUser(User user){
        userRepository.updateUser(user.getUsername(), user.getEmail(), user.getFirstname(), user.getLastname());
    }
    
    public void deactiveUser(User user){
        userRepository.deactiveUser(user.getUsername(), Constants.DEACTIVE);
    }
    
    public void activeUser(String username){
        userRepository.activeUser(username, Constants.ACTIVE);
    }
    public void promoteUser(String username){
        userRepository.promoteUser(username, Constants.ROLE_ADMIN);
    }
    public void demoteUser(String username){
        userRepository.demoteUser(username, Constants.ROLE_USER);
    }
    
    public void changePassword(User user){
        userRepository.changePassword(user.getUsername(), bCryptPasswordEncoder.encode(user.getPassword()));
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
    
    public void deleteUserByAdmin(User user){
        userRepository.delete(user);
    }
    public List<User> getAllUser(){
        return userRepository.findAll();
    }
}
