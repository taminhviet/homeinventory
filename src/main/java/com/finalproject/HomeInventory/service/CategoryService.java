package com.finalproject.HomeInventory.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalproject.HomeInventory.model.Category;
import com.finalproject.HomeInventory.repository.CategoryRepository;

@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;
    
    public Category findCategory(int CategoryId){
        Optional<Category> category = categoryRepository.findById(CategoryId);
        return category.get();
    }
    
    public List<Category> getAllCategories(){
        return categoryRepository.findAll();
    }
    
    public void saveCategory(Category category){
        categoryRepository.save(category);
    }
}
